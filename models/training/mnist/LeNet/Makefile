GPU_TRAINING=0
NETWORK=LeNet
PROBLEM=mnist

.SUFFIXES: .bestmodel .caffemodel .topology
.PRECIOUS: %.bestmodel
.PHONY: snapshot-rankings.txt clean $(NETWORK).plt *.dat

deep-clean: clean
	rm -f *.cpp *.caffemodel *.bestmodel *.txt

clean:
	rm -f active-solver.prototxt
	rm -rf *.topology *.dot *.png *.pdf *.plt *.dat
	rm -f *_iter*.caffemodel *_iter*.solverstate *.caffemodel *.solverstate

snapshot-rankings.txt:
	cat caffe-output.txt | egrep "Snapshotting to binary proto| top-1" | grep "Test" -B1 | grep "Snapshotting" -A1 | sed 's/^.*] Snapshotting to binary proto file //g' | sed 's/^.*output \#.*: top-1 =//g' | grep -v ']' | paste - - | grep active-solver | tee snapshot-rankings.txt

$(NETWORK).bestmodel: snapshot-rankings.txt
	@$(eval BEST_SNAPSHOT=$(shell cat snapshot-rankings.txt | sort -k2 --numeric --reverse | awk '{print $$1}' | head -n1))
	PYTHONPATH+=:/usr/python/ python -c "import caffe; net = caffe.Net(\"../../../deployment/$(PROBLEM)/$(NETWORK)/$(NETWORK)-deploy.prototxt\", caffe.TEST, weights=\"$(BEST_SNAPSHOT)\"); net.save(\"$(NETWORK).bestmodel\")"

$(NETWORK).caffemodel: $(NETWORK)-train.prototxt $(NETWORK)-solver.prototxt
	@echo "Beginning training run -- tail -f caffe-output.txt for live logs"
	@cp $(NETWORK)-solver.prototxt active-solver.prototxt
	@[[ 0 == $(GPU_TRAINING) ]] && true || echo "solver_mode: GPU" >> active-solver.prototxt
	@$(eval SOLVER_STATE=$(shell find *.solverstate -quit &> /dev/null && echo '-snapshot=`ls *.solverstate | sort --version-sort --reverse | head -n1`'))
	PYTHONPATH+=:/usr/python/ stdbuf -oL -eL caffe train -solver=active-solver.prototxt $(shell [[ 0 != $(GPU_TRAINING) ]] && echo '-gpu all') $(SOLVER_STATE) 2>caffe-output.txt
	@cp `ls *.caffemodel | sort --version-sort --reverse | head -n1` $(NETWORK).caffemodel
	@PYTHONPATH+=:/usr/python/ caffe test -model=../../../deployment/$(PROBLEM)/$(NETWORK)/$(NETWORK)-deploy.prototxt -weights=$(NETWORK).caffemodel -iterations=10 $(shell [[ 0 != $(GPU_TRAINING) ]] && echo '-gpu all')

$(NETWORK)-TOP1.dat:
	cat caffe-output.txt | egrep "Snapshotting to binary proto| top-1" | grep "Test" -B1 | grep "Snapshotting" -A1 | sed 's/^.*] Snapshotting to binary proto file //g' | sed 's/^.*output \#.*: top-1 =//g' | grep -v ']' | paste - - | grep active-solver | sed 's/active-solver_iter_//g' | sed 's/.caffemodel//g' > $(NETWORK)-TOP1.dat

$(NETWORK)-TOP1-MAX.dat:
	tail -n1 $(NETWORK)-TOP1.dat | awk '{printf "%s ",  $$1}' > $(NETWORK)-TOP1-MAX.dat
	cat caffe-output.txt | egrep "Snapshotting to binary proto| top-1" | grep "Test" -B1 | grep "Snapshotting" -A1 | sed 's/^.*] Snapshotting to binary proto file //g' | sed 's/^.*output \#.*: top-1 =//g' | grep -v ']' | paste - - | grep active-solver | sed 's/active-solver_iter_//g' | sed 's/.caffemodel//g' | sort --numeric --reverse -k2 | head -n1 | awk '{print $$2}' >> $(NETWORK)-TOP1-MAX.dat

$(NETWORK)-LR.dat:
	cat caffe-output.txt | grep " lr =" | awk '{print $$6, $$9}' | sed 's/,//g' > $(NETWORK)-LR.dat

$(NETWORK).plt:
	echo "set terminal pdf" > $(NETWORK).plt
	echo "set output \"$(NETWORK).pdf\"" >> $(NETWORK).plt
	echo "set yrange [0:]" >> $(NETWORK).plt
	echo "set ylabel \"top-1\"" >> $(NETWORK).plt
	echo "set xlabel \"Training Iteration\"" >> $(NETWORK).plt
	echo "set key above" >> $(NETWORK).plt
	echo "set xtics rotate by -45" >> $(NETWORK).plt
	echo "plot \"$(NETWORK)-TOP1.dat\" with linespoints, \\" >> $(NETWORK).plt
	echo "     \"$(NETWORK)-LR.dat\" with linespoints,   \\" >> $(NETWORK).plt
	echo "     \"$(NETWORK)-TOP1-MAX.dat\" using (0):2:1:(column(0)) notitle with vectors nohead dt 2 lc rgb \"black\"" >> $(NETWORK).plt

$(NETWORK).pdf: $(NETWORK)-TOP1.dat $(NETWORK)-TOP1-MAX.dat $(NETWORK)-LR.dat $(NETWORK).plt
	gnuplot $(NETWORK).plt

$(NETWORK).dot:
	PYTHONPATH+=:/usr/python/ GLOG_minloglevel=2 triNNity-compiler --backend optimizer-info --model ../../../deployment/$(PROBLEM)/$(NETWORK)/$(NETWORK)-deploy.prototxt --graph-output=$(NETWORK).dot

$(NETWORK).png: $(NETWORK).dot
	dot $(NETWORK).dot -Tpng -o $(NETWORK).png

$(NETWORK).topology:
	PYTHONPATH+=:/usr/python/ GLOG_minloglevel=2 triNNity-compiler --backend optimizer-info --model ../../../deployment/$(PROBLEM)/$(NETWORK)/$(NETWORK)-deploy.prototxt --topology-output=$(NETWORK).topology

