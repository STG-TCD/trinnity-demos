#include <chrono>
#include <iostream>
#include <fstream>
#include <algorithm>

#include <trinnity-compiler/runtimes/triNNity.hpp>
#include "imagenet.h"
#include "imagenet-impl.hpp"

int main(int argc, char **argv) {

  std::string weights_dir;
  std::string input_file;
  std::string output_file;

  bool benchmarking_mode = false;

  if (argc != 4) {
    std::cerr << "Usage: " << argv[0] << " <weights directory> <input> <output>" << std::endl;
    std::cerr << "Running in benchmarking mode since no input provided" << std::endl;
    benchmarking_mode = true;
  } else {
    weights_dir = std::string(argv[1]);
    input_file = std::string(argv[2]);
    output_file = std::string(argv[3]);

    std::cerr << "Loading weights from " << weights_dir << std::endl;
    std::cerr << "Loading input from " << input_file << std::endl;
    std::cerr << "Output to " << output_file << std::endl;
  }

  unsigned times[NO_OF_RUNS];

  Network<ACTIVATION_TYPE, WEIGHT_TYPE> net(weights_dir, output_file, input_file, benchmarking_mode);

  net.setup();

  std::cerr << "Allocating working space: " << net.working_space() << " bytes" << std::endl;

  net.load_input();

  auto t1 = std::chrono::high_resolution_clock::now();

  for (unsigned i = 0; i < NO_OF_RUNS; i++) {
    net.infer();
    auto t2 = std::chrono::high_resolution_clock::now();
    times[i] = std::chrono::duration_cast<std::chrono::nanoseconds>(t2-t1).count();
    t1 = t2;
  }

  for (unsigned i = 0; i < NO_OF_RUNS; i++) {
    std::cout << times[i] << std::endl;
  }

  net.save_output();

//net.teardown();

  return 0;
}
