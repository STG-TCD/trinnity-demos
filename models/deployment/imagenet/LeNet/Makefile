TRINNITY_COMPILER=triNNity-compiler
COST_MODEL=*i7-8700K*
COST_MODEL_VAR=*single*
RUNS=25
GPU_BENCHMARKING=0
GPU_TRAINING=0
GPU=all
CORES=4
CORE_IDS=0,1,2,3
ARCH_FLAGS=-march=native -mtune=native -Wall -Wno-error=coverage-mismatch --pedantic -fopenmp -faligned-new -fno-stack-protector
OPT_FLAGS=-O3 -flto -fwhole-program
OPT_FLAGS_UNSAFE=-Ofast -fgcse-lm -fgcse-sm -fgcse-las -fipa-pta -floop-nest-optimize -floop-parallelize-all -flto -fwhole-program -funsafe-loop-optimizations -funsafe-math-optimizations -ffast-math -ffp-contract=fast
CROSS_ARCH_FLAGS=-static -Wall -Wno-error=coverage-mismatch --pedantic -fopenmp -faligned-new -fno-stack-protector
CROSS_PREFIX=aarch64-linux-gnu
CROSS_CXX=$(CROSS_PREFIX)-g++
CROSS_STRIP=$(CROSS_PREFIX)-strip
CROSS_CPU=cortex-a57

BLAS_LIB=libopenblas.so
CROSS_BLAS_LIB=libopenblas.a

CROSS_ARMCL_PREFIX=/usr/aarch64-linux-gnu/
CROSS_ARMCL_SUPPORT_INCLUDE=$(CROSS_ARMCL_PREFIX)/include/arm_compute/
CROSS_ARMCL_LIB = -L$(CROSS_ARMCL_PREFIX)/lib/ -l:libarm_compute_core-static.a -l:libarm_compute-static.a
CROSS_ARMCL_FLAGS = -I $(CROSS_ARMCL_SUPPORT_INCLUDE) $(CROSS_ARMCL_LIB)

ARMCL_SUPPORT_INCLUDE = /usr/include/arm_compute/
ARMCL_LIB = -lOpenCL -ldl -Wl,--allow-shlib-undefined /usr/lib/arm_compute/CL*.o -l:libarm_compute.so -l:libarm_compute_core.so
ARMCL_FLAGS = -I $(ARMCL_SUPPORT_INCLUDE) $(ARMCL_LIB)

MKLDNN_MKL_INCLUDE=/usr/include/mkldnn/include/
MKLDNN_MKL_LIBDIR=/usr/lib/mkldnn/lib/
MKLDNN_LIB = -lmkldnn
MKLDNN_MKL_LIB = -lmklml_intel
MKLDNN_MKL_FLAGS = -I $(MKLDNN_MKL_INCLUDE) -L $(MKLDNN_MKL_LIBDIR)
MKLDNN_FLAGS = $(MKLDNN_MKL_FLAGS) $(MKLDNN_LIB) $(MKLDNN_MKL_LIB) -DTRINNITY_CBLAS_ROW_MAJOR -DTRINNITY_USE_CBLAS_GEMM -DTRINNITY_USE_CBLAS_GEMV -lopenblas

TRINNITY_FLAGS = -DTRINNITY_USE_GCC_VECTOR_EXTS -DTRINNITY_USE_CBLAS_GEMM -DTRINNITY_USE_CBLAS_GEMV

FLAGS_base = -DTRINNITY_NO_DEALLOCATE_LAYERS -DTRINNITY_DISABLE_LAYER_TIMING -DTRINNITY_CBLAS_ROW_MAJOR

FLAGS_triNNity=$(FLAGS_base) $(TRINNITY_FLAGS) -DTRINNITY_ENABLE_SPECTRAL_GENERIC_LAYER
CROSS_FLAGS_triNNity=$(FLAGS_triNNity)
OPT_FLAGS_triNNity=-DTRINNITY_NO_ASSERTS -fno-rtti -DTRINNITY_NO_EXCEPTIONS -fno-exceptions

FLAGS_triNNity-heuristic=$(FLAGS_base) $(TRINNITY_FLAGS) -DTRINNITY_ENABLE_SPECTRAL_GENERIC_LAYER
CROSS_FLAGS_triNNity-heuristic=$(FLAGS_triNNity-heuristic)
OPT_FLAGS_triNNity-heuristic=-DTRINNITY_NO_ASSERTS -fno-rtti -DTRINNITY_NO_EXCEPTIONS -fno-exceptions

FLAGS_triNNity-patch-gemm=$(FLAGS_base) $(TRINNITY_FLAGS) -DTRINNITY_ENABLE_SPECTRAL_GENERIC_LAYER
CROSS_FLAGS_triNNity-patch-gemm=$(FLAGS_triNNity-patch-gemm)
OPT_FLAGS_triNNity-patch-gemm=-DTRINNITY_NO_ASSERTS -fno-rtti -DTRINNITY_NO_EXCEPTIONS -fno-exceptions

FLAGS_triNNity-sum2d=$(FLAGS_base) $(TRINNITY_FLAGS)
CROSS_FLAGS_triNNity-sum2d=$(FLAGS_triNNity-sum2d)
OPT_FLAGS_triNNity-sum2d=-DTRINNITY_NO_ASSERTS -fno-rtti -DTRINNITY_NO_EXCEPTIONS -fno-exceptions

FLAGS_mkldnn=$(FLAGS_base) -DTRINNITY_WRAP_MKLDNN $(MKLDNN_FLAGS)
CROSS_FLAGS_mkldnn=$(FLAGS_mkldnn)
OPT_FLAGS_mkldnn=-DTRINNITY_NO_ASSERTS -fno-rtti -DTRINNITY_NO_EXCEPTIONS -fno-exceptions

FLAGS_armcl=$(FLAGS_base) -DTRINNITY_WRAP_ARMCL $(ARMCL_FLAGS)
CROSS_FLAGS_armcl=$(FLAGS_base) -DTRINNITY_WRAP_ARMCL $(CROSS_ARMCL_FLAGS)
OPT_FLAGS_armcl=-DTRINNITY_NO_ASSERTS

NETWORK=LeNet
CHALLENGE=imagenet

.SUFFIXES: .bestmodel .caffemodel .layers .constraints .topology .pbqp .solution .hsolution .small .pgo .pgo-sample .multi.dat .projected.dat .dat
.PRECIOUS: %.bestmodel %.exe %.small.exe %.pgo %.small.pgo %.cross.exe %.cross.small.exe %.cross.pgo %.cross.small.pgo
.PHONY: snapshot-rankings.txt clean

deep-clean: clean
	rm -f *.cpp *.caffemodel *.bestmodel *.txt

clean:
	rm -f *.s
	rm -f active-solver.prototxt
	rm -f *.exe
	rm -rf *.constraints *.layers *.topology *.*solution *.hpp *.h *.exe *.profile *.pbqp *.pgo *.pgo-sample *.dat *.txt *.parameters *.dot *.png
	rm -f *_iter*.caffemodel *_iter*.solverstate *.caffemodel *.solverstate

$(NETWORK).constraints: $(NETWORK)-deploy.prototxt
	PYTHONPATH+=:/usr/python/ GLOG_minloglevel=2 $(TRINNITY_COMPILER) --backend optimizer-info --model $(NETWORK)-deploy.prototxt --constraints-output=$(NETWORK).constraints

$(NETWORK).dot: $(NETWORK)-deploy.prototxt
	PYTHONPATH+=:/usr/python/ GLOG_minloglevel=2 $(TRINNITY_COMPILER) --backend optimizer-info --model $(NETWORK)-deploy.prototxt --graph-output=$(NETWORK).dot

$(NETWORK).png: $(NETWORK).dot $(NETWORK)-deploy.prototxt
	dot $(NETWORK).dot -Tpng -o $(NETWORK).png

$(NETWORK).parameters: $(NETWORK)-deploy.prototxt
	mkdir -p $(NETWORK).parameters
	PYTHONPATH+=:/usr/python/ GLOG_minloglevel=2 $(TRINNITY_COMPILER) --model $(NETWORK)-deploy.prototxt --weights $(NETWORK).caffemodel --data-output=$(NETWORK).parameters

$(NETWORK).layers: $(NETWORK)-deploy.prototxt
	PYTHONPATH+=:/usr/python/ GLOG_minloglevel=2 $(TRINNITY_COMPILER) --backend optimizer-info --model $(NETWORK)-deploy.prototxt --layers-output=$(NETWORK).layers

$(NETWORK).topology: $(NETWORK)-deploy.prototxt
	PYTHONPATH+=:/usr/python/ GLOG_minloglevel=2 $(TRINNITY_COMPILER) --backend optimizer-info --model $(NETWORK)-deploy.prototxt --topology-output=$(NETWORK).topology

$(NETWORK).pbqp: $(NETWORK).constraints $(NETWORK).layers $(NETWORK).topology
	triNNity-optimizer pbqp \
           -t $(NETWORK).topology \
           -s $(NETWORK).constraints \
           -a ../../../../libraries/triNNity/triNNity-algorithms.csv \
           -n $(shell printf "%q" ../../../../cost-models/$(COST_MODEL)/$(CHALLENGE)/triNNity/$(COST_MODEL_VAR)/hns.csv) \
           -e $(shell printf "%q" ../../../../cost-models/$(COST_MODEL)/$(CHALLENGE)/triNNity/$(COST_MODEL_VAR)/hns-transforms.csv) \
           > $(NETWORK).pbqp

$(NETWORK).solution: $(NETWORK).pbqp
	pbqp_solve $(NETWORK).pbqp h > $(NETWORK).solution

$(NETWORK).hsolution:
	triNNity-optimizer heuristic \
           -x LOCAL_OPTIMAL -t $(NETWORK).topology \
           -a ../../../../libraries/triNNity/triNNity-algorithms.csv \
           -n $(shell printf "%q" ../../../../cost-models/$(COST_MODEL)/$(CHALLENGE)/triNNity/$(COST_MODEL_VAR)/hns.csv) \
           > $(NETWORK).hsolution

$(NETWORK).dsolution:
	triNNity-optimizer heuristic \
           -x SUM2D_CHW -t $(NETWORK).topology \
           -a ../../../../libraries/triNNity/triNNity-algorithms.csv \
           -n /dev/null \
           > $(NETWORK).dsolution

$(NETWORK).psolution:
	triNNity-optimizer heuristic \
           -x PATCH_GEMM -t $(NETWORK).topology \
           -a ../../../../libraries/triNNity/triNNity-algorithms.csv \
           -n $(shell printf "%q" ../../../../cost-models/$(COST_MODEL)/$(CHALLENGE)/triNNity/$(COST_MODEL_VAR)/hns.csv) \
           > $(NETWORK).psolution

armcl.h: $(NETWORK).layers $(NETWORK).topology
	triNNity-optimizer instantiate -t $(NETWORK).topology \
                 -a ../../../../libraries/triNNity/triNNity-algorithms.csv \
                 -m ../../../../libraries/triNNity/triNNity-methods.csv \
                 -s /dev/null \
                 -i ARMCL \
                 -l $(NETWORK).layers \
                 > armcl.h

lpdnn.h: $(NETWORK).layers $(NETWORK).topology $(NETWORK).hsolution
	triNNity-optimizer instantiate -t $(NETWORK).topology \
                 -a ../../../../libraries/triNNity/triNNity-algorithms.csv \
                 -m ../../../../libraries/triNNity/triNNity-methods.csv \
                 -s $(NETWORK).hsolution \
                 -i LPDNN \
                 -l $(NETWORK).layers \
                 > lpdnn.h

mkldnn.h: $(NETWORK).layers $(NETWORK).topology
	triNNity-optimizer instantiate -t $(NETWORK).topology \
                 -a ../../../../libraries/triNNity/triNNity-algorithms.csv \
                 -m ../../../../libraries/triNNity/triNNity-methods.csv \
                 -s /dev/null \
                 -i MKLDNN \
                 -l $(NETWORK).layers \
                 > mkldnn.h

triNNity.h: $(NETWORK).layers $(NETWORK).topology $(NETWORK).solution
	triNNity-optimizer instantiate -t $(NETWORK).topology \
                 -a ../../../../libraries/triNNity/triNNity-algorithms.csv \
                 -m ../../../../libraries/triNNity/triNNity-methods.csv \
                 -s $(NETWORK).solution \
                 -i TRINNITY \
                 -l $(NETWORK).layers \
                 > triNNity.h

triNNity-heuristic.h: $(NETWORK).layers $(NETWORK).topology $(NETWORK).hsolution
	triNNity-optimizer instantiate -t $(NETWORK).topology \
                 -a ../../../../libraries/triNNity/triNNity-algorithms.csv \
                 -m ../../../../libraries/triNNity/triNNity-methods.csv \
                 -s $(NETWORK).hsolution \
                 -i TRINNITY \
                 -l $(NETWORK).layers \
                 > triNNity-heuristic.h

triNNity-patch-gemm.h: $(NETWORK).layers $(NETWORK).topology $(NETWORK).psolution
	triNNity-optimizer instantiate -t $(NETWORK).topology \
                 -a ../../../../libraries/triNNity/triNNity-algorithms.csv \
                 -m ../../../../libraries/triNNity/triNNity-methods.csv \
                 -s $(NETWORK).psolution \
                 -i TRINNITY \
                 -l $(NETWORK).layers \
                 > triNNity-patch-gemm.h

triNNity-sum2d.h: $(NETWORK).layers $(NETWORK).topology $(NETWORK).dsolution
	triNNity-optimizer instantiate -t $(NETWORK).topology \
                 -a ../../../../libraries/triNNity/triNNity-algorithms.csv \
                 -m ../../../../libraries/triNNity/triNNity-methods.csv \
                 -s $(NETWORK).dsolution \
                 -i TRINNITY \
                 -l $(NETWORK).layers \
                 > triNNity-sum2d.h

armcl-impl.hpp: $(NETWORK).constraints $(NETWORK).layers $(NETWORK).topology armcl.h
	PYTHONPATH+=:/usr/python/ GLOG_minloglevel=2 $(TRINNITY_COMPILER) --backend armcl --model $(NETWORK)-deploy.prototxt --code-output=armcl-impl.hpp

mkldnn-impl.hpp: $(NETWORK).constraints $(NETWORK).layers $(NETWORK).topology mkldnn.h
	PYTHONPATH+=:/usr/python/ GLOG_minloglevel=2 $(TRINNITY_COMPILER) --backend mkldnn --model $(NETWORK)-deploy.prototxt --code-output=mkldnn-impl.hpp

triNNity-impl.hpp: $(NETWORK).constraints $(NETWORK).layers $(NETWORK).topology triNNity.h
	PYTHONPATH+=:/usr/python/ GLOG_minloglevel=2 $(TRINNITY_COMPILER) --backend trinnity --model $(NETWORK)-deploy.prototxt --code-output=triNNity-impl.hpp

triNNity-heuristic-impl.hpp: $(NETWORK).constraints $(NETWORK).layers $(NETWORK).topology triNNity-heuristic.h
	PYTHONPATH+=:/usr/python/ GLOG_minloglevel=2 $(TRINNITY_COMPILER) --backend trinnity --model $(NETWORK)-deploy.prototxt --code-output=triNNity-heuristic-impl.hpp

triNNity-patch-gemm-impl.hpp: $(NETWORK).constraints $(NETWORK).layers $(NETWORK).topology triNNity-patch-gemm.h
	PYTHONPATH+=:/usr/python/ GLOG_minloglevel=2 $(TRINNITY_COMPILER) --backend trinnity --model $(NETWORK)-deploy.prototxt --code-output=triNNity-patch-gemm-impl.hpp

triNNity-sum2d-impl.hpp: $(NETWORK).constraints $(NETWORK).layers $(NETWORK).topology triNNity-sum2d.h
	PYTHONPATH+=:/usr/python/ GLOG_minloglevel=2 $(TRINNITY_COMPILER) --backend trinnity --model $(NETWORK)-deploy.prototxt --code-output=triNNity-sum2d-impl.hpp

tflow.py:
	PYTHONPATH+=:/usr/python/ GLOG_minloglevel=2 $(TRINNITY_COMPILER) --backend tensorflow --model $(NETWORK)-deploy.prototxt --code-output=tflow.py

%.pgo: $(NETWORK).layers $(NETWORK).topology %-impl.hpp %.h %.pgo-sample
	OMP_NUM_THREADS=$(CORES) MKL_NUM_THREADS=$(CORES) taskset -c $(CORE_IDS) ./$*.pgo-sample | head -n$(RUNS) | sort --version-sort > $*.pgo-sample.dat
	ln -sf $*.h $(CHALLENGE).h
	ln -sf $*-impl.hpp $(CHALLENGE)-impl.hpp
	$(CXX) -fprofile-use=$*.profile --std=c++17 $(ARCH_FLAGS) $(OPT_FLAGS) $(FLAGS_$*) $(OPT_FLAGS_$*) -DNO_OF_RUNS=$(RUNS) -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float main.cpp -lpthread -l:$(BLAS_LIB) -o $*.pgo && strip $*.pgo

%.small.pgo: $(NETWORK).layers $(NETWORK).topology %-impl.hpp %.h %.small.pgo-sample
	OMP_NUM_THREADS=$(CORES) MKL_NUM_THREADS=$(CORES) taskset -c $(CORE_IDS) ./$*.small.pgo-sample | head -n$(RUNS) | sort --version-sort > $*.small.pgo-sample.dat
	ln -sf $*.h $(CHALLENGE).h
	ln -sf $*-impl.hpp $(CHALLENGE)-impl.hpp
	$(CXX) -fprofile-use=$*.profile --std=c++17 $(ARCH_FLAGS) -Os $(FLAGS_$*) -DNO_OF_RUNS=$(RUNS) -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float main.cpp -lpthread -l:$(BLAS_LIB) -o $*.small.pgo && strip $*.small.pgo

%.exe: $(NETWORK).layers $(NETWORK).topology %-impl.hpp %.h
	ln -sf $*.h $(CHALLENGE).h
	ln -sf $*-impl.hpp $(CHALLENGE)-impl.hpp
	$(CXX) --std=c++17 $(ARCH_FLAGS) $(OPT_FLAGS) $(FLAGS_$*) $(OPT_FLAGS_$*) -DNO_OF_RUNS=$(RUNS) -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float main.cpp -lpthread -l:$(BLAS_LIB) -o $*.exe && strip $*.exe

%.pgo-sample: $(NETWORK).layers $(NETWORK).topology %-impl.hpp %.h
	ln -sf $*.h $(CHALLENGE).h
	ln -sf $*-impl.hpp $(CHALLENGE)-impl.hpp
	$(CXX) -fprofile-generate=$*.profile --std=c++17 $(ARCH_FLAGS) $(OPT_FLAGS) $(FLAGS_$*) $(OPT_FLAGS_$*) -DNO_OF_RUNS=1 -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float main.cpp -lpthread -l:$(BLAS_LIB) -o $*.pgo-sample

%.debug.exe: $(NETWORK).layers $(NETWORK).topology %-impl.hpp %.h
	ln -sf $*.h $(CHALLENGE).h
	ln -sf $*-impl.hpp $(CHALLENGE)-impl.hpp
	$(CXX) --std=c++17 $(ARCH_FLAGS) -O0 -ggdb3 $(FLAGS_$*) -DNO_OF_RUNS=$(RUNS) -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float main.cpp -lpthread -l:$(BLAS_LIB) -o $*.debug.exe

%.small.exe: $(NETWORK).layers $(NETWORK).topology %-impl.hpp %.h
	ln -sf $*.h $(CHALLENGE).h
	ln -sf $*-impl.hpp $(CHALLENGE)-impl.hpp
	$(CXX) --std=c++17 $(ARCH_FLAGS) -Os $(FLAGS_$*) -DNO_OF_RUNS=$(RUNS) -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float main.cpp -lpthread -l:$(BLAS_LIB) -o $*.small.exe && strip $*.small.exe

%.small.pgo-sample: $(NETWORK).layers $(NETWORK).topology %-impl.hpp %.h
	ln -sf $*.h $(CHALLENGE).h
	ln -sf $*-impl.hpp $(CHALLENGE)-impl.hpp
	$(CXX) -fprofile-generate=$*.profile --std=c++17 $(ARCH_FLAGS) -Os $(FLAGS_$*) -DNO_OF_RUNS=1 -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float main.cpp -lpthread -l:$(BLAS_LIB) -o $*.small.pgo-sample

%.cross.pgo: $(NETWORK).layers $(NETWORK).topology %-impl.hpp %.h %.cross.pgo-sample
	-ssh $(CROSS_BENCHMARK_HOST) mkdir -p $(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/
	-rsync --quiet -avP --delete --ignore-errors ./ $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/$(NETWORK)/ && ssh -t $(CROSS_BENCHMARK_HOST) "cd $(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/$(NETWORK) && OMP_NUM_THREADS=$(CORES) taskset -c $(CORE_IDS) ./$*.cross.pgo-sample | head -n$(RUNS) | sort --version-sort > $*.cross.pgo-sample.dat"
	ln -sf $*.h $(CHALLENGE).h
	ln -sf $*-impl.h $(CHALLENGE)-impl.h
	$(CROSS_CXX) -mcpu=$(CROSS_CPU) -fprofile-use=cross-$*.profile --std=c++17 $(CROSS_ARCH_FLAGS) $(OPT_FLAGS) $(CROSS_FLAGS_$*) $(OPT_FLAGS_$*) -DNO_OF_RUNS=$(RUNS) -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float main.cpp -lpthread -l:$(CROSS_BLAS_LIB) -o $*.cross.pgo && $(CROSS_STRIP) $*.cross.pgo
	-ssh $(CROSS_BENCHMARK_HOST) mkdir -p $(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/
	-rsync --quiet -avP --delete --ignore-errors ./ $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/$(NETWORK)/

%.cross.small.pgo: $(NETWORK).layers $(NETWORK).topology %-impl.hpp %.h %.cross.small.pgo-sample
	-ssh $(CROSS_BENCHMARK_HOST) mkdir -p $(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/
	-rsync --quiet -avP --delete --ignore-errors ./ $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/$(NETWORK)/ && ssh -t $(CROSS_BENCHMARK_HOST) "cd $(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/$(NETWORK) && OMP_NUM_THREADS=$(CORES) taskset -c $(CORE_IDS) ./$*.cross.small.pgo-sample | head -n$(RUNS) | sort --version-sort > $*.cross.small.pgo-sample.dat"
	ln -sf $*.h $(CHALLENGE).h
	ln -sf $*-impl.h $(CHALLENGE)-impl.h
	$(CROSS_CXX) -mcpu=$(CROSS_CPU) -fprofile-use=$*.profile --std=c++17 $(CROSS_ARCH_FLAGS) -Os $(CROSS_FLAGS_$*) -DNO_OF_RUNS=$(RUNS) -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float main.cpp -lpthread -l:$(CROSS_BLAS_LIB) -o $*.cross.small.pgo && $(CROSS_STRIP) $*.cross.small.pgo
	-ssh $(CROSS_BENCHMARK_HOST) mkdir -p $(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/
	-rsync --quiet -avP --delete --ignore-errors ./ $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/$(NETWORK)/

%.cross.exe: $(NETWORK).layers $(NETWORK).topology %-impl.hpp %.h
	ln -sf $*.h $(CHALLENGE).h
	ln -sf $*-impl.h $(CHALLENGE)-impl.h
	$(CROSS_CXX) -mcpu=$(CROSS_CPU) --std=c++17 $(CROSS_ARCH_FLAGS) $(OPT_FLAGS) $(CROSS_FLAGS_$*) $(OPT_FLAGS_$*) -DNO_OF_RUNS=$(RUNS) -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float main.cpp -lpthread -l:$(CROSS_BLAS_LIB) -o $*.cross.exe && $(CROSS_STRIP) $*.cross.exe
	-ssh $(CROSS_BENCHMARK_HOST) mkdir -p $(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/
	-rsync --quiet -avP --delete --ignore-errors ./ $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/$(NETWORK)/

%.cross.pgo-sample: $(NETWORK).layers $(NETWORK).topology %-impl.hpp %.h
	ln -sf $*.h $(CHALLENGE).h
	ln -sf $*-impl.h $(CHALLENGE)-impl.h
	$(CROSS_CXX) -mcpu=$(CROSS_CPU) -fprofile-generate=cross-$*.profile --std=c++17 $(CROSS_ARCH_FLAGS) $(OPT_FLAGS) $(CROSS_FLAGS_$*) $(OPT_FLAGS_$*) -DNO_OF_RUNS=1 -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float main.cpp -lpthread -l:$(CROSS_BLAS_LIB) -o $*.cross.pgo-sample
	-ssh $(CROSS_BENCHMARK_HOST) mkdir -p $(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/
	-rsync --quiet -avP --delete --ignore-errors ./ $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/$(NETWORK)/

%.cross.debug.exe: $(NETWORK).layers $(NETWORK).topology %-impl.hpp %.h
	ln -sf $*.h $(CHALLENGE).h
	ln -sf $*-impl.h $(CHALLENGE)-impl.h
	$(CROSS_CXX) -mcpu=$(CROSS_CPU) --std=c++17 $(CROSS_ARCH_FLAGS) -O0 -ggdb3 $(CROSS_FLAGS_$*) -DNO_OF_RUNS=$(RUNS) -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float main.cpp -lpthread -l:$(CROSS_BLAS_LIB) -o $*.cross.debug.exe
	-ssh $(CROSS_BENCHMARK_HOST) mkdir -p $(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/
	-rsync --quiet -avP --delete --ignore-errors ./ $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/$(NETWORK)/

%.cross.small.exe: $(NETWORK).layers $(NETWORK).topology %-impl.hpp %.h
	ln -sf $*.h $(CHALLENGE).h
	ln -sf $*-impl.h $(CHALLENGE)-impl.h
	$(CROSS_CXX) -mcpu=$(CROSS_CPU) --std=c++17 $(CROSS_ARCH_FLAGS) -Os $(CROSS_FLAGS_$*) -DNO_OF_RUNS=$(RUNS) -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float main.cpp -lpthread -l:$(CROSS_BLAS_LIB) -o $*.cross.small.exe && $(CROSS_STRIP) $*.cross.small.exe
	-ssh $(CROSS_BENCHMARK_HOST) mkdir -p $(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/
	-rsync --quiet -avP --delete --ignore-errors ./ $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/$(NETWORK)/

%.cross.small.pgo-sample: $(NETWORK).layers $(NETWORK).topology %-impl.hpp %.h
	ln -sf $*.h $(CHALLENGE).h
	ln -sf $*-impl.h $(CHALLENGE)-impl.h
	$(CROSS_CXX) -mcpu=$(CROSS_CPU) -fprofile-generate=$*.profile --std=c++17 $(CROSS_ARCH_FLAGS) -Os $(CROSS_FLAGS_$*) -DNO_OF_RUNS=1 -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float main.cpp -lpthread -l:$(CROSS_BLAS_LIB) -o $*.cross.small.pgo-sample
	-ssh $(CROSS_BENCHMARK_HOST) mkdir -p $(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/
	-rsync --quiet -avP --delete --ignore-errors ./ $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/$(NETWORK)/

mkldnn.exe: $(NETWORK).layers $(NETWORK).topology mkldnn.cpp mkldnn.h
	ln -sf mkldnn.h $(NETWORK).h
	$(CXX) --std=c++17 $(ARCH_FLAGS) $(OPT_FLAGS) $(FLAGS_mkldnn) -DNO_OF_RUNS=$(RUNS) -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float mkldnn.cpp -o mkldnn.exe && strip mkldnn.exe

mkldnn.small.exe: $(NETWORK).layers $(NETWORK).topology mkldnn.cpp mkldnn.h
	ln -sf mkldnn.h $(NETWORK).h
	$(CXX) --std=c++17 $(ARCH_FLAGS) -Os $(FLAGS_mkldnn) -DNO_OF_RUNS=$(RUNS) -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float mkldnn.cpp -o mkldnn.small.exe && strip mkldnn.small.exe

%.s: $(NETWORK).layers $(NETWORK).topology %-impl.hpp %.h
	ln -sf $*.h $(CHALLENGE).h
	ln -sf $*-impl.hpp $(CHALLENGE)-impl.hpp
	$(CXX) --std=c++17 $(ARCH_FLAGS) $(OPT_FLAGS) $(FLAGS_$*) $(OPT_FLAGS_$*) -DNO_OF_RUNS=$(RUNS) -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float main.cpp -S -o $*.s

%.small.s: $(NETWORK).layers $(NETWORK).topology %-impl.hpp %.h
	ln -sf $*.h $(CHALLENGE).h
	ln -sf $*-impl.hpp $(CHALLENGE)-impl.hpp
	$(CXX) --std=c++17 $(ARCH_FLAGS) -Os $(FLAGS_$*) -DNO_OF_RUNS=$(RUNS) -DWEIGHT_TYPE=float -DACTIVATION_TYPE=float main.cpp -S -o $*.small.s

caffe.dat: $(NETWORK)-deploy.prototxt
	$(eval CAFFE_GPU_MODE=$(shell [[ 0 == $(GPU_BENCHMARKING) ]] && true || echo "-gpu $(GPU)"))
	OMP_NUM_THREADS=$(CORES) MKL_NUM_THREADS=$(CORES) taskset -c $(CORE_IDS) caffe time $(CAFFE_GPU_MODE) -model=$(NETWORK)-deploy.prototxt -weights=$(NETWORK).caffemodel -iterations=$(RUNS) 2>&1 | tee caffe-output.txt
	cat caffe-output.txt | grep "Average Forward pass" | awk '{print (1e6 * $$8)}' > caffe-time.txt
	rm -f caffe.dat && for i in `seq $(RUNS)`; do cat caffe-time.txt >> caffe.dat; done

triNNity.projected.dat: $(NETWORK).solution
	tail -n2 $(NETWORK).solution | head -n1 > projected-time.txt
	rm -f triNNity.projected.dat && for i in `seq $(RUNS)`; do cat projected-time.txt >> triNNity.projected.dat; done
	rm -f projected-time.txt

triNNity-heuristic.projected.dat: $(NETWORK).hsolution
	tail -n2 $(NETWORK).hsolution | head -n1 > heuristic-projected-time.txt
	rm -f triNNity-heuristic.projected.dat && for i in `seq $(RUNS)`; do cat heuristic-projected-time.txt >> triNNity-heuristic.projected.dat; done
	rm -f heuristic-projected-time.txt

triNNity-sum2d.projected.dat: $(NETWORK).dsolution
	tail -n2 $(NETWORK).dsolution | head -n1 > sum2d-projected-time.txt
	rm -f triNNity-sum2d.projected.dat && for i in `seq $(RUNS)`; do cat sum2d-projected-time.txt >> triNNity-sum2d.projected.dat; done
	rm -f sum2d-projected-time.txt

%.dat: %.exe
	OMP_NUM_THREADS=$(CORES) MKL_NUM_THREADS=$(CORES) taskset -c $(CORE_IDS) ./$*.exe | head -n$(RUNS) | sort --version-sort > $*.dat

%.pgo.dat: %.pgo
	OMP_NUM_THREADS=$(CORES) MKL_NUM_THREADS=$(CORES) taskset -c $(CORE_IDS) ./$*.pgo | head -n$(RUNS) | sort --version-sort > $*.pgo.dat

%.small.dat: %.small.exe
	OMP_NUM_THREADS=$(CORES) MKL_NUM_THREADS=$(CORES) taskset -c $(CORE_IDS) ./$*.small.exe | head -n$(RUNS) | sort --version-sort > $*.small.dat

%.small.pgo.dat: %.small.pgo
	OMP_NUM_THREADS=$(CORES) MKL_NUM_THREADS=$(CORES) taskset -c $(CORE_IDS) ./$*.small.pgo | head -n$(RUNS) | sort --version-sort > $*.small.pgo.dat

%.cross.dat: %.cross.exe
	ssh -t $(CROSS_BENCHMARK_HOST) "cd $(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/$(NETWORK) && OMP_NUM_THREADS=$(CORES) taskset -c $(CORE_IDS) ./$*.cross.exe | head -n$(RUNS) | sort --version-sort > $*.cross.dat"
	-rsync --quiet -avP $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/$(NETWORK)/ ./

%.cross.pgo.dat: %.cross.pgo
	ssh -t $(CROSS_BENCHMARK_HOST) "cd $(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/$(NETWORK) && OMP_NUM_THREADS=$(CORES) taskset -c $(CORE_IDS) ./$*.cross.pgo | head -n$(RUNS) | sort --version-sort > $*.cross.pgo.dat"
	-rsync --quiet -avP $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/$(NETWORK)/ ./

%.cross.small.dat: %.cross.small.exe
	ssh -t $(CROSS_BENCHMARK_HOST) "cd $(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/$(NETWORK) && OMP_NUM_THREADS=$(CORES) taskset -c $(CORE_IDS) ./$*.cross.small.exe | head -n$(RUNS) | sort --version-sort > $*.cross.small.dat"
	-rsync --quiet -avP $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/$(NETWORK)/ ./

%.cross.small.pgo.dat: %.cross.small.pgo
	ssh -t $(CROSS_BENCHMARK_HOST) "cd $(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/$(NETWORK) && OMP_NUM_THREADS=$(CORES) taskset -c $(CORE_IDS) ./$*.cross.small.pgo | head -n$(RUNS) | sort --version-sort > $*.cross.small.pgo.dat"
	-rsync --quiet -avP $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/$(CHALLENGE)/$(NETWORK)/ ./
