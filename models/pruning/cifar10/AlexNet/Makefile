GPU_TRAINING=0
NETWORK=AlexNet

.SUFFIXES: .bestmodel .caffemodel .topology
.PRECIOUS: %.bestmodel
.PHONY: snapshot-rankings.txt clean

deep-clean: clean
	rm -f *.cpp *.caffemodel *.bestmodel *.txt

clean:
	rm -f active-solver.prototxt
	rm -rf *.topology *.dot *.png
	rm -f *_iter*.caffemodel *_iter*.solverstate *.caffemodel *.solverstate

snapshot-rankings.txt:
	cat caffe-output.txt | egrep "Snapshotting to binary proto| top-1" | grep "Test" -B1 | grep "Snapshotting" -A1 | sed 's/^.*] Snapshotting to binary proto file //g' | sed 's/^.*output \#1: top-1 =//g' | grep -v ']' | paste - - | grep active-solver | tee snapshot-rankings.txt

$(NETWORK).bestmodel: snapshot-rankings.txt
	@$(eval BEST_SNAPSHOT=$(shell cat snapshot-rankings.txt | sort -k2 --numeric --reverse | awk '{print $$1}' | head -n1))
	PYTHONPATH+=:/usr/python/ python -c "import caffe; net = caffe.Net(\"../../../deployment/cifar10/$(NETWORK)/$(NETWORK)-deploy.prototxt\", caffe.TEST, weights=\"$(BEST_SNAPSHOT)\"); net.save(\"$(NETWORK).bestmodel\")"

$(NETWORK).caffemodel: $(NETWORK)-train.prototxt $(NETWORK)-solver.prototxt
	@echo "Beginning training run -- tail -f caffe-output.txt for live logs"
	@cp $(NETWORK)-solver.prototxt active-solver.prototxt
	@[[ 0 == $(GPU_TRAINING) ]] && true || echo "solver_mode: GPU" >> active-solver.prototxt
	@$(eval SOLVER_STATE=$(shell find *.solverstate -quit &> /dev/null && echo '-snapshot=`ls *.solverstate | sort --version-sort --reverse | head -n1`'))
	PYTHONPATH+=:/usr/python/ stdbuf -oL -eL caffe train -solver=active-solver.prototxt $(shell [[ 0 != $(GPU_TRAINING) ]] && echo '-gpu all') $(SOLVER_STATE) 2>caffe-output.txt
	@cp `ls *.caffemodel | sort --version-sort --reverse | head -n1` $(NETWORK).caffemodel
	@PYTHONPATH+=:/usr/python/ caffe test -model=$(NETWORK)-deploy.prototxt -weights=$(NETWORK).caffemodel -iterations=10 $(shell [[ 0 != $(GPU_TRAINING) ]] && echo '-gpu all')

$(NETWORK).prunedmodel: $(NETWORK)-train.prototxt $(NETWORK)-solver.prototxt
	@echo "Beginning pruning run -- tail -f pruning-caffe-output.txt for live logs"
	@cp $(NETWORK)-solver.prototxt active-solver.prototxt
	@[[ 0 == $(GPU_TRAINING) ]] && true || echo "solver_mode: GPU" >> active-solver.prototxt
	PYTHONPATH+=:/usr/python/ python ../../prune_channels.py --solver=active-solver.prototxt --pretrained $(NETWORK).bestmodel --pruned-model $(NETWORK)-pruned --retrain true --saliency-pointwise TAYLOR --saliency-reduction L2 --train-size 80 --drop-acc 0.2 2>caffe-output.txt
	mv $(NETWORK)-pruned.caffemodel $(NETWORK).prunedmodel

$(NETWORK).dot: $(NETWORK)-deploy.prototxt
	PYTHONPATH+=:/usr/python/ GLOG_minloglevel=2 $(TRINNITY_COMPILER) --backend optimizer-info --model $(NETWORK)-deploy.prototxt --graph-output=$(NETWORK).dot

$(NETWORK).png: $(NETWORK).dot $(NETWORK)-deploy.prototxt
	dot $(NETWORK).dot -Tpng -o $(NETWORK).png

$(NETWORK).topology: $(NETWORK)-deploy.prototxt
	PYTHONPATH+=:/usr/python/ GLOG_minloglevel=2 $(TRINNITY_COMPILER) --backend optimizer-info --model $(NETWORK)-deploy.prototxt --topology-output=$(NETWORK).topology

