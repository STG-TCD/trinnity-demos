#!/usr/bin/env python3
import os
import numpy as np
import cv2
import shutil
from subprocess import call

if __name__ == "__main__":

  mnist_train_archive = os.path.abspath("mnist_train.tar.gz")
  mnist_test_archive = os.path.abspath("mnist_test.tar.gz")

  if not os.path.exists(mnist_train_archive):
    print("Downloading MNIST...")
    call(
      "wget https://pjreddie.com/media/files/mnist_train.tar.gz",
      shell=True
    )

  if not os.path.exists(mnist_test_archive):
    call(
      "wget https://pjreddie.com/media/files/mnist_test.tar.gz",
      shell=True
    )

  print("Downloading done.\n")

  print("Extracting...")

  call(
    "tar -zxvf mnist_train.tar.gz",
    shell=True
  )

  call(
    "tar -zxvf mnist_test.tar.gz",
    shell=True
  )

  print("Extraction finished")

  print("Converting...")

  mnist_caffe_directory = os.path.abspath('./data')

  if not os.path.exists(mnist_caffe_directory):
    os.makedirs(mnist_caffe_directory)

  filenames_train = os.listdir('train')

  X_train = [x.split('-')[0] for x in filenames_train]
  y_train = [x.split('-')[1].split('.')[0][3:] for x in filenames_train]

  train_index = 0
  train_listing = ""
  for (fname, (img, label)) in zip(filenames_train, zip([x for x in X_train], [l for l in y_train])):
    new_fname = "train-{}.png".format(train_index)
    shutil.move(os.path.join('train', fname), os.path.join(mnist_caffe_directory, new_fname))
    train_listing += mnist_caffe_directory + "/" + new_fname + " " + str(label) + "\n"
    train_index += 1

  with open(os.path.join(mnist_caffe_directory, "train-index.txt"), "w") as text_file:
    text_file.write(train_listing)

  filenames_test = os.listdir('test')

  X_test = [x.split('-')[0] for x in filenames_test]
  y_test = [x.split('-')[1].split('.')[0][3:] for x in filenames_test]

  test_index = 0
  test_listing = ""
  for (fname, (img, label)) in zip(filenames_test, zip([x for x in X_test], [l for l in y_test])):
    new_fname = "test-{}.png".format(test_index)
    shutil.move(os.path.join('test', fname), os.path.join(mnist_caffe_directory, new_fname))
    test_listing += mnist_caffe_directory + "/" + new_fname + " " + str(label) + "\n"
    test_index += 1

  with open(os.path.join(mnist_caffe_directory, "test-index.txt"), "w") as text_file:
    text_file.write(test_listing)

  with open(os.path.join(mnist_caffe_directory, "labels.txt"), "w") as text_file:
    text_file.write("\n".join(map(str, list(range(0,10)))))

  print("Cleaning up...")

  shutil.rmtree('train')
  shutil.rmtree('test')

  print("Done")
