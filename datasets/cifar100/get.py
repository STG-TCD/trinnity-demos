#!/usr/bin/env python3
import copy
import os
from subprocess import call
import numpy as np
import sklearn.model_selection
import pickle
import cv2
import shutil

def unpickle(file):
    fo = open(file, 'rb')
    dict = pickle.load(fo, encoding='latin-1')
    fo.close()
    return dict

def load_data(train_batches):
    data = []
    fine_labels = []
    coarse_labels = []
    for data_batch_i in train_batches:
        d = unpickle(
            os.path.join(cifar_python_directory, data_batch_i)
        )
        data.append(d['data'])
        fine_labels.append(np.array(d['fine_labels']))
        coarse_labels.append(np.array(d['coarse_labels']))
    # Merge training batches on their first dimension
    data = np.concatenate(data)
    fine_labels = np.concatenate(fine_labels)
    coarse_labels = np.concatenate(coarse_labels)
    length = len(fine_labels)

    return data.reshape(length, 3, 32, 32), fine_labels, coarse_labels

def load_label_names():
  d = unpickle (
      os.path.join(cifar_python_directory, "meta")
  )
  return d

if __name__ == "__main__":

  cifar_python_archive = os.path.abspath("cifar-100-python.tar.gz")

  if not os.path.exists(cifar_python_archive):
    print("Downloading CIFAR100...")
    call(
      "wget http://www.cs.toronto.edu/~kriz/cifar-100-python.tar.gz",
      shell=True
    )
    print("Downloading done.\n")

  cifar_python_directory = os.path.abspath("cifar-100-python")

  print("Extracting...")

  call(
    "tar -zxvf cifar-100-python.tar.gz",
    shell=True
  )

  print("Extracting successfully done to {}".format(cifar_python_directory))

  print("Converting...")
  
  cifar_caffe_directory = os.path.abspath('./data')

  X, y_fine, y_coarse = load_data(
      ["train"]
  )

  Xt, yt_fine, yt_coarse = load_data(
      ["test"]
  )

  L = load_label_names()

  if not os.path.exists(cifar_caffe_directory):
    os.makedirs(cifar_caffe_directory)

  train_index = 0
  train_fine_listing = ""
  train_coarse_listing = ""
  for (img, fine_label, coarse_label) in zip([x for x in X], [l1 for l1 in y_fine], [l2 for l2 in y_coarse]):
    fname = "train-{}.png".format(train_index)
    r = img[0].astype(np.uint8)
    g = img[1].astype(np.uint8)
    b = img[2].astype(np.uint8)
    output = cv2.merge((b, g, r))
    # output.reshape((32, 32, 3))
    cv2.imwrite(os.path.join(cifar_caffe_directory, fname), output)
    train_fine_listing += cifar_caffe_directory + "/" + fname + " " + str(fine_label) + "\n"
    train_coarse_listing += cifar_caffe_directory + "/" + fname + " " + str(coarse_label) + "\n"
    train_index += 1

  with open(os.path.join(cifar_caffe_directory, "train-fine-index.txt"), "w") as text_file:
    text_file.write(train_fine_listing)

  with open(os.path.join(cifar_caffe_directory, "train-coarse-index.txt"), "w") as text_file:
    text_file.write(train_coarse_listing)

  test_index = 0
  test_fine_listing = ""
  test_coarse_listing = ""
  for (img, fine_label, coarse_label) in zip([x for x in Xt], [l1 for l1 in yt_fine], [l2 for l2 in yt_coarse]):
    fname = "test-{}.png".format(test_index)
    r = img[0].astype(np.uint8)
    g = img[1].astype(np.uint8)
    b = img[2].astype(np.uint8)
    output = cv2.merge((b, g, r))
    # output.reshape((32, 32, 3))
    cv2.imwrite(os.path.join(cifar_caffe_directory, fname), output)
    test_fine_listing += cifar_caffe_directory + "/" + fname + " " + str(fine_label) + "\n"
    test_coarse_listing += cifar_caffe_directory + "/" + fname + " " + str(coarse_label) + "\n"
    test_index += 1

  with open(os.path.join(cifar_caffe_directory, "test-fine-index.txt"), "w") as text_file:
    text_file.write(test_fine_listing)

  with open(os.path.join(cifar_caffe_directory, "test-coarse-index.txt"), "w") as text_file:
    text_file.write(test_coarse_listing)

  with open(os.path.join(cifar_caffe_directory, "fine_labels.txt"), "w") as text_file:
    text_file.write("\n".join(L['fine_label_names']))

  with open(os.path.join(cifar_caffe_directory, "coarse_labels.txt"), "w") as text_file:
    text_file.write("\n".join(L['coarse_label_names']))

  print("Cleaning up...")

  shutil.rmtree(cifar_python_directory)

  print("Done")
