#!/usr/bin/env python3
import copy
import os
from subprocess import call
import numpy as np
import sklearn.model_selection
import pickle
import cv2
import shutil

def unpickle(file):
    fo = open(file, 'rb')
    dict = pickle.load(fo)
    fo.close()
    return dict

def load_data(train_batches):
    data = []
    labels = []
    for data_batch_i in train_batches:
        d = unpickle(
            os.path.join(imagenet_python_directory, data_batch_i)
        )
        data.append(d['data'])
        labels.append(np.array(d['labels']))
    # Merge training batches on their first dimension
    data = np.concatenate(data)
    labels = np.concatenate(labels)
    length = len(labels)

    return data.reshape(length, 3, 32, 32), labels

def load_label_names():
  d = unpickle (
      os.path.join(imagenet_python_directory, "batches.meta")
  )
  return d

if __name__ == "__main__":

  imagenet_python_archive = os.path.abspath("imagenet.tar.gz")

  if not os.path.exists(imagenet_python_archive):
    print("Downloading Imagenet...")
    call(
      "wget http://image-net.org/download-images/imagenet-32x32-train.tar.gz",
      shell=True
    )
    print("Downloading done.\n")

  imagenet_python_directory = os.path.abspath("imagenet-batches-py")

  print("Extracting...")

  call(
    "tar -zxvf imagenet-32x32-train.tar.gz",
    shell=True
  )

  print("Extracting successfully done to {}".format(imagenet_python_directory))

  print("Converting...")

  imagenet_caffe_directory = os.path.abspath('./data')

  X, y = load_data(
      ["data_batch_{}".format(i) for i in range(1, 6)]
  )

  Xt, yt = load_data(["test_batch"])

  L = load_label_names()

  if not os.path.exists(imagenet_caffe_directory):
    os.makedirs(imagenet_caffe_directory)

  train_index = 0
  train_listing = ""
  for (img, label) in zip([x for x in X], [l for l in y]):
    fname = "train-{}.png".format(train_index)
    r = img[0].astype(np.uint8)
    g = img[1].astype(np.uint8)
    b = img[2].astype(np.uint8)
    output = cv2.merge((b, g, r))
    # output.reshape((32, 32, 3))
    cv2.imwrite(os.path.join(imagenet_caffe_directory, fname), output)
    train_listing += imagenet_caffe_directory + "/" + fname + " " + str(label) + "\n"
    train_index += 1

  with open(os.path.join(imagenet_caffe_directory, "train-index.txt"), "w") as text_file:
    text_file.write(train_listing)

  test_index = 0
  test_listing = ""
  for (img, label) in zip([x for x in Xt], [l for l in yt]):
    fname = "test-{}.png".format(test_index)
    r = img[0].astype(np.uint8)
    g = img[1].astype(np.uint8)
    b = img[2].astype(np.uint8)
    output = cv2.merge((b, g, r))
    # output.reshape((32, 32, 3))
    cv2.imwrite(os.path.join(imagenet_caffe_directory, fname), output)
    test_listing += imagenet_caffe_directory + "/" + fname + " " + str(label) + "\n"
    test_index += 1

  with open(os.path.join(imagenet_caffe_directory, "test-index.txt"), "w") as text_file:
    text_file.write(test_listing)

  with open(os.path.join(imagenet_caffe_directory, "labels.txt"), "w") as text_file:
    text_file.write("\n".join(L['label_names']))

  print("Cleaning up...")

  shutil.rmtree(imagenet_python_directory)

  print("Done")
