# A Walkthrough of the triNNity toolchain

This document contains a guided tour around the triNNity toolchain using a
simple network and classification challenge which should be possible without a
GPU. For this guide, we focus on the `mnist` challenge, and use the `LeNet`
CNN.

We cover initial training and testing of the model, cost modelling,
optimization, and code generation, and finally whole-network benchmarking
versus Caffe. We assume that you are running these commands in one of the
docker environments provided in the Dockerfiles in this repo.

## Training the Model

Using the scripts in the datasets directories, you can fetch the training data
for different challenges. Fetch the training data for the `mnist` challenge.

```
cd datasets/mnist
./get.py
```

Train LeNet using 4 CPU cores.

```
cd ../../models/training/mnist/LeNet
taskset -c 0-3 make -B LeNet.caffemodel GPU_TRAINING=0
make LeNet.bestmodel
```

To see a live log of what's happening during training, hit `Ctrl-z` to suspend
the process, and type `bg` to resume it in the background, then run `tail -f
caffe-output.txt | grep solver`. To see a live graph of the training progress,
`make LeNet.pdf`.

This sequence of commands fetches the dataset, and trains the LeNet model for
mnist using `triNNity-caffe`. The final step processes all of the snapshots
generated during the training process and ranks them to find the best version
of the model that was produced during training. This is saved as
`LeNet.bestmodel`, but is a normal `.caffemodel` file.

A series of snapshots will be generated during the training process. These are
catalogued in the file `snapshot-rankings.txt`. To view the top 3 snapshots by
accuracy, you can say

```
sort -k2 --numeric-sort snapshot-rankings.txt --reverse | head -n3
```


## Generating an optimized model for inference

This phase has multiple steps. First, we need to build a cost model that will
drive the `triNNity-compiler` optimizer when generating the code to implement
the network. The compiler is run in analysis mode to process the networks
available for the challenge (in this case, `mnist`), and build a table of all
of the unique convolution layers that it sees. These will be timed using
different convolution algorithms to build the cost model. We also build a table
of all tensor transformations that may be required, since these also need to be
timed to accurately model the cost of the network.

```
cd models/benchmarking
make mnist-scenarios mnist-transforms
```

These files are used to drive the microbenchmarking process in the
`triNNity-benchmarks` project.

### Microbenchmarking

Change to the `triNNity-benchmarks` project after running the above commands.

```
rm -f Makefile.config && touch Makefile.config
make end-to-end-singlethreaded-mnist-x86_64
make end-to-end-multithreaded-mnist-x86_64
```

This will launch the microbenchmarking process. Eventually, CSV files
containing the timings of different convolution algorithms for all unique
layers in the networks used for the challenge will be produced. Look for the
CSV files in the `stats` folder. Have a look at `Makefile.config.mnist-x86_64`
to see what is being done, or run `make info` for a summary.

If you are cross-compiling the benchmarks to run them on another system, simply
prefix these commands with `cross-`, for example:

```
make cross-end-to-end-singlethreaded-mnist-a53
```

You will need `make`, `sudo`, and `rsync` installed on the remote machine,
which can be configured with the `CROSS_BENCHMARK_HOST` variable in the
corresponding `Makefile.config.*` file.

### Optimization

The `triNNity-demos` repository comes with several pregenerated cost models for
different hardware platforms in the `cost-models` directory, which just consist
of the two CSV files with the algorithm and data transformation timings. Let's
use one of these to generate an optimized `LeNet` using `triNNity-compiler`.

```
cd models/benchmarking/mnist/LeNet
make -B triNNity-patch-gemm.exe RUNS=1
make -B triNNity-sum2d.exe RUNS=1
```

The compiler and optimizer commands to produce the two versions of the network
are printed by the Makefile.

Next, we need to make sure that the optimized versions of the network are
correct. Using the provided comparison script in the repository, we can compare
the outputs of the different optimized versions of the network with Caffe.

First we need to dump the network weights in a format that the
`triNNity-compiler` generated executables can use. We'll use the `LeNet.bestmodel`
that we trained earlier.

```
cp ../../../training/mnist/LeNet/LeNet.bestmodel LeNet.caffemodel
make LeNet.parameters
```

Now we can use the provided comparison script to check a set of inputs against
Caffe.

```
PYTHONPATH=/usr/python GLOG_minloglevel=2 ../../../../tools/compare.py -d mnist -x 28 -y 28 -p LeNet-deploy.prototxt -m LeNet.caffemodel -l ../../../../datasets/mnist/data/labels.txt -g ../../../../datasets/mnist/data/test-index.txt -i ../../../../datasets/mnist/data/ -o /tmp/triNNity-output.bin -e $(realpath triNNity-sum2d.exe) -w LeNet.parameters -t 5 -c 100
```

The command above checks the `triNNity-sum2d.exe` version of the network, but
you can replace that by `triNNity-patch-gemm.exe` to check this version as
well.

### Benchmarking

The final step is to gather some whole-network benchmarking data with the
optimized version of the network. There are rules in the Makefile to run the
executables in benchmarking mode, but for convenience, a higher level Makefile
has rules to run all the version of each network in any of the challenges,
since the benchmark data is only useful when there's something to be compared.

```
cd ../../
make triNNity-sum2d-benchmarks-mnist triNNity-patch-gemm-benchmarks-mnist caffe-benchmarks-mnist
make -B mnist.csv
```

After running the whole-network benchmarking, `mnist.csv` should contain a
summary of the runtimes of the different versions of the `mnist` networks.

