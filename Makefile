.PHONY: update update-caffe-cpu update-caffe-gpu all datasets clean

update:
	yay -Syy trinnity-optimizer-git --answerclean A --answerdiff N
	yay -Syy trinnity-compiler-git --answerclean A --answerdiff N
	yay -Syy trinnity-git --answerclean A --answerdiff N

update-caffe-cpu:
	yay -Syy trinnity-caffe-git --answerclean A --answerdiff N

update-caffe-gpu:
	yay -Syy trinnity-caffe-cuda-git --answerclean A --answerdiff N

clean: datasets-clean
	$(MAKE) -C models/deployment clean
	$(MAKE) -C models/pruning clean
	$(MAKE) -C models/quantization clean
	$(MAKE) -C models/training clean

# Fetch the datasets for training
datasets: cifar10-datasets cifar100-datasets imagenet-datasets mnist-datasets

%-datasets:
	@cd datasets/$* && python get.py

datasets-clean:
	@rm -rf datasets/*/*.gz datasets/*/data

check-models-%:
	@for x in `ls ../triNNity-models/$*`; do echo $$x; cd models/training/$*/$$x && caffe test -model $$x-train.prototxt -weights ../../../../../triNNity-models/$*/$$x/$$x.bestmodel -iterations 10 2>&1 | egrep 'top(.?)1' | tail -n1 && cd -; done
