#!/usr/bin/env python3
from enum import Enum
from io import BytesIO
from scipy.io.wavfile import read
from PIL import Image
import numpy as np
import subprocess
import argparse
import imutils
import time
import cv2
import threading
import queue
import librosa
import os

class Backend(Enum):
  caffe_cpu = 'caffe-cpu'
  caffe_gpu = 'caffe-gpu'
  cv_myriad = 'opencv-myriad'
  cv_opencl = 'opencv-opencl'
  cv_cpu = 'opencv-cpu'

  def __str__(self):
    return self.value

class Input(Enum):
  wav = 'wav'
  mic = 'mic'

  def __str__(self):
    return self.value

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--prototxt", required=True,
  help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", required=True,
  help="path to Caffe pre-trained model")
ap.add_argument("-l", "--labels", required=True,
  help="path to dataset labels file")
ap.add_argument("-d", "--device", type=int,
  help="which audio device to use to capture input data")
ap.add_argument("-w", "--wav", type=str,
  help="which audio file to use for input data")
ap.add_argument("-s", "--seconds", type=float, required=True,
  help="what length of audio to capture (seconds)")
ap.add_argument("-b", "--backend", type=Backend, default=False, choices=list(Backend),
  help="which backend should be used")
ap.add_argument("-t", "--type", type=Input, default=False, choices=list(Input),
  help="which type of input is being provided")
args = vars(ap.parse_args())

# initialize the list of class labels
# then generate a set of colors for each class
CLASSES = []

with open(args["labels"], "r") as classes_file:
  CLASSES = [line.strip() for line in classes_file.readlines()]

COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

net = None

if 'opencv' in args["backend"].value:
  # load our serialized model from disk
  print("[INFO] loading model...")
  net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])

  # specify the target device as the Myriad processor on the NCS
  if args["backend"].value == 'opencv-myriad':
    net.setPreferableTarget(cv2.dnn.DNN_TARGET_MYRIAD)
  elif args["backend"].value == 'opencv-opencl':
    net.setPreferableTarget(cv2.dnn.DNN_TARGET_OPENCL)
  elif args["backend"].value == 'opencv-cpu':
    net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)
  else:
    raise ArgumentError("Unknown backend requested: " + str(args["backend"]))
elif 'caffe' in args["backend"].value:
  import caffe
  # load our serialized model from disk
  print("[INFO] loading model...")
  net = caffe.Net(args["prototxt"], args["model"], caffe.TEST)

  # specify the target device as the Myriad processor on the NCS
  if args["backend"].value == 'caffe-cpu':
    caffe.set_mode_cpu()
  elif args["backend"].value == 'caffe-gpu':
    caffe.set_mode_gpu()
  else:
    raise ArgumentError("Unknown backend requested: " + str(args["backend"]))
else:
  raise ArgumentError("Unknown backend requested: " + str(args["backend"]))

q = queue.Queue(maxsize=8)
finished = False

def audio_grabber_mic():
  seed = 42
  DEVNULL = open(os.devnull, 'wb')
  while (not finished):
    wavfile = '/tmp/capture_frame_'+str(seed)+'.wav'
    subprocess.call(['ffmpeg', '-y', '-f', 'alsa', '-i', 'hw:'+str(args["device"]), '-t', str(args["seconds"]), '-acodec', 'pcm_s16le', '-ac', '1', '-ar', '16000', wavfile], stdout=DEVNULL, stderr=DEVNULL)
    q.put(wavfile)
    print("Captured audio frame #"+str(seed))
    seed += 1

def audio_grabber_wav():
  raise NotImplementedError

def compute_mfcc(wavFilePath, imgPathNormalised, imgPathRaw):
  n_freq = 40 # number of frequency bands
  n_window = 32 # number of time slices
  s_rate = 16000 # audio sample rate in WAV files

  f = open(wavFilePath, "rb")
  buf = BytesIO(f.read())
  wav = read(buf)
  x = np.array(wav[1], dtype=float)
  mfccs = librosa.feature.mfcc(x, sr=s_rate, n_mfcc=n_freq)
  f.close()

  # check mfcc dimensions
  if mfccs.shape[0] != n_freq:
    print("MFCC matrix has non-standard frequency bands: %d" % mfccs.shape[0])
    sys.exit(1)

  if mfccs.shape[1] < n_window:
    t = n_window - mfccs.shape[1]
    p = np.zeros(shape=(n_freq, t))
    mfccs = np.concatenate((mfccs, p), axis=1)
  elif mfccs.shape[1] > n_window:
    mfccs = mfccs[:, 0:n_window]

  mfcc_data = mfccs[:, :]

  img = Image.fromarray(np.asarray(np.clip(mfcc_data, 0, 255), dtype="uint8"), "L")
  img.save(imgPathRaw)

  mfcc_max_intensity = np.max(mfcc_data)
  mfcc_data = (mfcc_data * 255.0) / (mfcc_max_intensity)

  img = Image.fromarray(np.asarray(np.clip(mfcc_data, 0, 255), dtype="uint8"), "L")
  img.save(imgPathNormalised)

def keyword_spotter():
  seed = 42
  while True:
    print("Waiting for data...")
    wavfile = q.get()
    imagefile_path = '/tmp/capture_frame_'+str(seed)+'.png'
    imagefile_raw_path = '/tmp/capture_frame_'+str(seed)+'.raw.png'
    imagefile = compute_mfcc(wavfile, imagefile_path, imagefile_raw_path)
    frame = cv2.imread(imagefile_path)
    frame2 = cv2.imread(imagefile_raw_path)

    # grab the frame dimensions and convert it to a blob
    (h, w) = frame.shape[:2]
    blob = cv2.dnn.blobFromImage(frame, 1, (w, h))

    frame2 = imutils.resize(frame2, width=400)

    # pass the blob through the network and obtain the detections and
    # predictions
    detections = None

    if 'opencv' in args["backend"].value:
      net.setInput(blob)
      detections = net.forward()
    elif 'caffe' in args["backend"].value:
      net.blobs['data'].data[0] = blob[0]
      net.forward()
      detections = net.blobs['prob'].data
    else:
      raise ArgumentError("Unknown execution engine requested " + str(args["backend"].value))

    predictions = detections.reshape([detections.shape[0], -1])
    # get the top 5 predictions
    idxs = np.argsort(predictions[0])[::-1][:5]

    # loop over the top 5 predictions
    for (i, idx) in enumerate(idxs):
      # draw the top prediction on the input image
      if predictions[0][idx] > 0.1:
        text = "Label: {}, {:.2f}%".format(CLASSES[idx],
          predictions[0][idx] * 100)
        red = 255 * (predictions[0][idx])
        green = 255 * (1 - predictions[0][idx])
        cv2.putText(frame2, text, (5, 25+25*i),  cv2.FONT_HERSHEY_SIMPLEX,
          0.7, (0, green, red), 2)

      # display the predicted label + associated probability to the
      # console
      print("[INFO] {}. label: {}, probability: {:.5}".format(i + 1,
        CLASSES[idx], predictions[0][idx]))

    # display the output image
    cv2.imshow("Image", frame2)

    key = cv2.waitKey(1) & 0xFF

    q.task_done()

    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
      finished = True
      break

if args["type"].value == 'mic':
  ag_thread = threading.Thread(target=audio_grabber_mic)
  kws_thread = threading.Thread(target=keyword_spotter)
  ag_thread.daemon = True
  kws_thread.daemon = True
  kws_thread.start()
  ag_thread.start()
  kws_thread.join()
elif args["type"].value == 'wav':
  ag_thread = threading.Thread(target=audio_grabber_wav)
  kws_thread = threading.Thread(target=keyword_spotter)
  ag_thread.daemon = True
  kws_thread.daemon = True
  kws_thread.start()
  ag_thread.start()
  kws_thread.join()
else:
  raise ArgumentError("Unknown option requested for input type: " + args["type"])

cv2.destroyAllWindows()

