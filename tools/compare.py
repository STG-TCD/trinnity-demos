#!/usr/bin/env python3
import numpy as np
import argparse
import random
import imutils
import time
import cv2
import os
import caffe
import subprocess
import tempfile

# Constants for dataset preprocessing

DATASETS = ['cifar10', 'mnist']

RGB_MEANS = dict([('cifar10', (125.7984, 123.4432, 114.304)),
                  ('cifar100', (129.8176, 124.5952, 112.8448)),
                  ('mnist', (0, 0, 0)),
                 ])

SCALE_FACTORS = dict([('cifar10', 0.00390625),
                      ('cifar100', 0.00390625),
                      ('mnist', 0.00390625),
                     ])

# Arguments
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--prototxt", required=True,
  help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", required=True,
  help="path to Caffe pre-trained model")
ap.add_argument("-l", "--labels", required=True,
  help="path to dataset labels file")
ap.add_argument("-g", "--ground-truth", required=True,
  help="path to ground truth labels file")
ap.add_argument("-i", "--input", required=True,
  help="path to input dataset")
ap.add_argument("-d", "--dataset", required=True,
  help="Name of input dataset")
ap.add_argument("-e", "--executable", required=False,
  help="path to triNNity executable")
ap.add_argument("-w", "--weights", required=False,
  help="path to dumped model weights data")
ap.add_argument("-t", "--top", type=int, default=1,
  help="check the top N predictions")
ap.add_argument("-x", "--width", type=int, required=True,
  help="network input width")
ap.add_argument("-y", "--height", type=int, required=True,
  help="network input height")
ap.add_argument("-c", "--count", type=int, required=True,
  help="how many images to use to evaluate")
ap.add_argument("--lpdnn-executable", required=False,
  help="path to lpdnn executable")
ap.add_argument("--lpdnn-config", required=False,
  help="path to lpdnn config")
ap.add_argument("--lpdnn-libdir", required=False,
  help="path to lpdnn libdir")
ap.add_argument("--show-output", action="store_true", default=False,
  help="display numerical classification values while testing")

def run_triNNity(args, input_path, input_blob, output_path):
  subprocess.call([args["executable"], args["weights"], input_blob, output_path],
                    stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

def run_lpdnn(args, input_path, input_blob, output_path):
  (fd, file_name) = tempfile.mkstemp()
  libpath = 'LD_LIBRARY_PATH=' + args["lpdnn_libdir"] + ":" + '${LD_LIBRARY_PATH}'
  pypath = 'PYTHONPATH=' + args["lpdnn_libdir"] + ":" + '${PYTHONPATH}'
  cmd = [args["lpdnn_executable"], "--cfg", args["lpdnn_config"], "-r", "1", "-f", os.path.join(args["input"], input_path), "|", "grep", "'confidence'", "|", "jq", "'.result.confidence'", "-c", "|", "tr",  "'[]'", "' '", ">", file_name]
  cmd_str = libpath + ' ' + pypath + ' ' + ' '.join(cmd)
  os.system(cmd_str)

  probs = []
  with open(file_name, "r") as filedata:
    probs = list(map(float, [x.strip().split(',') for x in filedata.readlines()][0]))

  os.close(fd)

  npdata = np.asarray(probs, dtype=np.float32)
  npdata.tofile(output_path)
  # print(npdata)

def compare_with_caffe(args, classes, net, competitor_name, run_competitor):
  # compute top-N match percentage and top-1 accuracy for both Caffe and competitor code
  # we can only do top-1 accuracy because most classification datasets use a 1-label ground truth
  top_n_matches = 0
  correct_agreement = 0
  incorrect_agreement = 0
  disagreement_caffe_correct = 0
  disagreement_competitor_correct = 0
  caffe_correct = 0
  competitor_correct = 0

  dataset_files = [x for x in os.listdir(args["input"]) if ".png" in x]
  ground_truth_data = dict()

  with open(args["ground_truth"], "r") as gtfile:
    ground_truth_data = dict(list(map(lambda a: (os.path.basename(a[0]), a[1]), [x.split() for x in gtfile.readlines()])))

  for idx in range(args["count"]):
    test_image = random.choice(list(ground_truth_data.keys()))
    imread_flags = cv2.IMREAD_COLOR
    if args["dataset"] == 'mnist':
      imread_flags = cv2.IMREAD_GRAYSCALE

    frame = cv2.imread(os.path.join(args["input"], test_image), imread_flags)

    # grab the frame dimensions and convert it to a blob
    blob = cv2.dnn.blobFromImage(frame, 1, (args["width"], args["height"]), RGB_MEANS[args["dataset"]]) * SCALE_FACTORS[args["dataset"]]

    # get the output from Caffe
    net.blobs['data'].data[0] = blob[0]
    net.forward()
    caffe_output = net.blobs['prob'].data.flatten()

    if args["show_output"]:
      print("caffe output: ", caffe_output)

    caffe_idxs = np.argsort(caffe_output)[::-1][:args["top"]]

    if args["show_output"]:
      print("caffe idxs: ", caffe_idxs)

    caffe_top_label = classes[caffe_idxs[0]]

    # loop over the top N predictions
    classes_caffe = set()
    for (i, idx) in enumerate(caffe_idxs):
      classes_caffe.add(classes[idx])
      prob_caffe = caffe_output[idx]

    # dump the input for the competitor executable
    (blob_fd, blob_file_name) = tempfile.mkstemp()
    (output_fd, output_file_name) = tempfile.mkstemp()

    blob[0].tofile(blob_file_name)

    # run the competitor executable
    run_competitor(args, test_image, blob_file_name, output_file_name)

    os.close(blob_fd)

    # get the output from competitor executable
    competitor_output = np.fromfile(output_file_name, dtype=np.float32, count=-1).flatten()

    os.close(output_fd)

    if args["show_output"]:
      print(competitor_name + " output: ", competitor_output)

    competitor_idxs = np.argsort(competitor_output)[::-1][:args["top"]]

    if args["show_output"]:
      print(competitor_name + " idxs: ", competitor_idxs)

    competitor_top_label = classes[competitor_idxs[0]]

    # loop over the top N predictions
    classes_competitor = set()
    for (i, idx) in enumerate(competitor_idxs):
      classes_competitor.add(classes[idx])
      prob_competitor = competitor_output[idx]

    if classes_caffe == classes_competitor:
      top_n_matches += 1

    if caffe_top_label == classes[int(ground_truth_data[test_image])]:
      caffe_correct += 1

    if competitor_top_label == classes[int(ground_truth_data[test_image])]:
      competitor_correct += 1

    if caffe_top_label == competitor_top_label and competitor_top_label == classes[int(ground_truth_data[test_image])]:
      correct_agreement += 1

    if caffe_top_label == competitor_top_label and competitor_top_label != classes[int(ground_truth_data[test_image])]:
      incorrect_agreement += 1

    if caffe_top_label != competitor_top_label and caffe_top_label == classes[int(ground_truth_data[test_image])]:
      disagreement_caffe_correct += 1

    if caffe_top_label != competitor_top_label and competitor_top_label == classes[int(ground_truth_data[test_image])]:
      disagreement_competitor_correct += 1

    print("Top " + str(args["top"]) + " labels for " + test_image + ", caffe: " + str([classes[idx] for idx in caffe_idxs]) + ", " + competitor_name + ": " + str([classes[idx] for idx in competitor_idxs]))

  print("caffe accuracy: " + str(caffe_correct/args["count"]))
  print(competitor_name + " accuracy: " + str(competitor_correct/args["count"]))
  print("top-" + str(args["top"]) + " agreement: " + str(top_n_matches/args["count"]))
  print("top-1 agreement (right): " + str(correct_agreement) + "/" + str(args["count"]))
  print("top-1 agreement (wrong): " + str(incorrect_agreement) + "/" + str(args["count"]))
  print("top-1 agreement (total): " + str(correct_agreement+incorrect_agreement) + "/" + str(args["count"]))
  print("top-1 disagreement (caffe right): " + str(disagreement_caffe_correct) + "/" + str(args["count"]))
  print("top-1 disagreement (caffe wrong): " + str(disagreement_competitor_correct) + "/" + str(args["count"]))
  print("top-1 disagreement (total): " + str(disagreement_caffe_correct+disagreement_competitor_correct) + "/" + str(args["count"]))

if __name__ == "__main__":
  args = vars(ap.parse_args())

  # initialize the list of class labels
  CLASSES = []
  with open(args["labels"], "r") as classes_file:
    CLASSES = [line.strip() for line in classes_file.readlines()]

  if args["dataset"] not in DATASETS:
    print("Unsupported dataset requested: " + args["dataset"] + " (available datasets: " + str(DATASETS))
    exit(1)

  caffe.set_mode_cpu()

  # load our serialized model from disk
  net = caffe.Net(args["prototxt"], args["model"], caffe.TEST)

  if args["executable"]:
    compare_with_caffe(args, CLASSES, net, 'triNNity', run_triNNity)

  if args["lpdnn_executable"]:
    compare_with_caffe(args, CLASSES, net, 'lpdnn', run_lpdnn)
