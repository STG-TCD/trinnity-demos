FROM archlinux/base

MAINTAINER Andrew Anderson

RUN pacman-key --refresh-keys

RUN pacman --quiet --noconfirm -Syyu base base-devel bind-tools git doxygen ghc wget bc gnuplot go python-numpy python-scipy bash-completion stack gtk3 openssh rsync

RUN groupadd build

RUN echo "%build ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

RUN echo "MAKEFLAGS=\"-j12\"" >> /etc/makepkg.conf

RUN useradd --create-home --groups build user

# Install yay

WORKDIR /tmp

RUN git clone https://aur.archlinux.org/yay.git

RUN chmod -R a+rw /tmp/yay

USER user

WORKDIR /tmp/yay

RUN makepkg -sfc

USER root

RUN pacman --noconfirm -U yay*.xz

WORKDIR /

# Set up repository and install required packages

RUN echo >> /etc/pacman.conf

RUN echo >> /etc/pacman.conf

RUN echo "[córais]" >> /etc/pacman.conf

RUN echo "SigLevel = Never" >> /etc/pacman.conf

RUN echo "Server = http://corais.scss.tcd.ie/repo/" >> /etc/pacman.conf

# Install openblas-lapack-openmp and other numerical libs

USER root

RUN pacman --quiet --noconfirm -Sy corais-boost corais-boost-libs corais-opencv

USER user

RUN yes Y | yay -Syy openblas-lapack-openmp --nocleanmenu --nodiffmenu --noeditmenu --noupgrademenu

# Set triNNity-optimizer prerequisites

USER user

RUN mkdir -p ~/.stack/

RUN echo > ~/.stack/config.yaml

RUN echo "allow-newer: true" >> ~/.stack/config.yaml

# Install Caffe dependencies

USER user

RUN yes Y | yay -Syy cmake cython gflags google-glog hdf5 ipython jsoncpp libaec libuv protobuf python-gflags python-jedi python-nose python-pandas python-parso python-pexpect python-pickleshare python-prompt_toolkit python-protobuf python-ptyprocess python-pygments python-pytz python-traitlets python-wcwidth python-yaml rhash

# Install openmp

USER user

RUN yes Y | yay -Syy openmp --noconfirm --nocleanmenu --nodiffmenu --noeditmenu --noupgrademenu

# Install pbqp solver

USER user

RUN yes Y | yay -Syy pbqp --noconfirm --nocleanmenu --nodiffmenu --noeditmenu --noupgrademenu

# Install scikit-learn for dataset management tasks

RUN yes Y | yay -Syy python-scikit-learn python-imutils




# Install triNNity tools and cuda

USER user

RUN yes Y | yay -Syy cuda cudnn --answerclean A --answerdiff N

RUN yes Y | yay -Syy trinnity-git --answerclean A --answerdiff N

RUN yes Y | yay -Syy trinnity-caffe-cudnn-git --answerclean A --answerdiff N

RUN yes Y | yay -Syy trinnity-compiler-git --answerclean A --answerdiff N

RUN yes Y | yay -Syy trinnity-optimizer-git --answerclean A --answerdiff N




# Copy triNNity-demos

USER user

ADD ./ /home/user/triNNity-demos/

WORKDIR /home/user/

RUN git clone https://bitbucket.org/STG-TCD/trinnity-benchmarks.git triNNity-benchmarks

USER root

RUN chown -R user:user /home/user/

USER user

ENV PATH="/opt/cuda/bin:${PATH}"

ENV PYTHONPATH="/usr/python:${PYTHONPATH}"
